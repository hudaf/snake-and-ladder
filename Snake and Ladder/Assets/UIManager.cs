using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIManager : MonoBehaviour
{
    [SerializeField] private GameManager gameManager;
    [SerializeField] private TextMeshProUGUI diceText;
    void Awake() 
    {
        gameManager.GetDiceValue += OnGetDiceValue;
    }
    void OnGetDiceValue(int i)
    {
        diceText.text = i.ToString();
    }
}

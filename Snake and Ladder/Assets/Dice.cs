using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Dice : MonoBehaviour
{
    [SerializeField] private List<DiceSide> sides; 
    public Rigidbody rb;
    [SerializeField] private float rotateSpeed;
    [SerializeField] private Vector3 readyPos;
    [SerializeField] private Vector3 rolledPos;
    public int diceValue;
    public event Action<int> SendValue;
    public event Action DiceReady;
    public bool isReady;
    public bool isRolling;

    private void Start() 
    {
        SetReady();

    }
    void Update()
    {
        if(isReady)
        {
            Rotate();
        }
        if(isRolling)
        {
            IdleCheck();
        }
    }

    void SetReady()
    {
        StartCoroutine(PositionLerp(0));
    }
    void Rotate()
    {
        transform.Rotate(Vector3.one*rotateSpeed*Time.deltaTime);
    }
    public void Roll()
    {
        isReady = false;
        rb.isKinematic = false;
        rb.AddForce(new Vector3(0,0,5f),ForceMode.Impulse);
        rb.velocity = new Vector3(0,0,0.1f);
        float rnd = UnityEngine.Random.Range(20f,50f);
        rb.AddTorque(new Vector3(rnd,rnd,rnd));
        isRolling = true;
    }
    public void IdleCheck()
    {
        if(rb.velocity.magnitude == 0f)
        {
            OnStopped();
        }
    }
    public void OnStopped()
    {
        for (int i = 0; i < sides.Count; i++)
        {
            if(sides[i].touching)
            {
                SendValue?.Invoke(sides[i].ID);
                isRolling = false;
            }
        }
        SetReady();
    }
    IEnumerator PositionLerp(float t)
    {
        while (Vector3.Distance(transform.position, readyPos) > 1f)
        {
            transform.position = Vector3.Lerp(transform.position, readyPos, Time.deltaTime*5);
            yield return null;
        }
        rb.isKinematic = true;
        isReady = true;
        DiceReady?.Invoke();
    }
    void OnDestroy() 
    {
        SendValue = null;
        DiceReady = null;
    }
}

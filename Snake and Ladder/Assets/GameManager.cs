using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class GameManager : MonoBehaviour
{
    private enum GameStates {Ready, Roll, Wait, Walk, End}
    [SerializeField] private TileSpawner tileSpawner;
    [SerializeField] private Player player;
    [SerializeField] private Dice dice;
    [SerializeField] private GameStates gameState;
    private int diceValue;
    public event Action<int> GetDiceValue;
    
    void Start()
    {
        dice.DiceReady += OnDiceReady;
        dice.SendValue += OnGetDiceValue;
    }

    void Update()
    {
        GameState();
    }
    void GameState()
    {
        switch(gameState)
        {
            case GameStates.Ready:
                Ready();
                break;
            case GameStates.Roll:
                Roll();
                break;
            case GameStates.Wait:
                Wait();
                break;
            case GameStates.Walk:
                break;
            case GameStates.End:
                break;
        }
    }

    void Ready()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            dice.Roll();
            gameState = GameStates.Roll;
        }
    }
    void Roll()
    {

    }
    void Wait()
    {

    }
    void Walk()
    {

    }
    void End()
    {

    }
    void OnGetDiceValue(int a)
    {
        diceValue = a;
        GetDiceValue?.Invoke(a);
        StartCoroutine(player.Walk(a));
    }
    void OnDiceReady()
    {
        gameState = GameStates.Ready;
    }
    void OnDestroy() {
        GetDiceValue = null;
    }
}

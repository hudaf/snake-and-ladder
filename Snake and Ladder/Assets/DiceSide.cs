using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceSide : MonoBehaviour
{
    public int ID;
    public bool touching;
    private void OnTriggerEnter(Collider other) 
    {
        if(other)
        {
            touching = true;
        }
    }
    private void OnTriggerExit(Collider other) 
    {
        if(other)
        {
            touching = false;
        }
    }
}

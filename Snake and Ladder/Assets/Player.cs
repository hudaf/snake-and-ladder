using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private TileSpawner tileSpawner;
    private int currentPos;
    public int targetPos;

    void Start()
    {
        currentPos = -1;
    }

    void Update()
    {
        
    }
    public IEnumerator Walk(int diceValue)
    {
        targetPos = currentPos + diceValue;
        var max = tileSpawner.tileList.Count-1;
        if(targetPos <= max)
        {
            while (transform.position != tileSpawner.tileList[targetPos].transform.position)
            {
                currentPos++;
                while(transform.position != tileSpawner.tileList[currentPos].transform.position)
                {
                    transform.position = Vector3.Lerp(transform.position, tileSpawner.tileList[currentPos].transform.position, Time.deltaTime * 50f);       
                    yield return null;
                }
                yield return null;
            }
        }
        else if(targetPos > max)
        {
            int temp = targetPos - max;
            targetPos = max;
            while (transform.position != tileSpawner.tileList[targetPos].transform.position)
            {
                currentPos++;
                while(transform.position != tileSpawner.tileList[currentPos].transform.position)
                {
                    transform.position = Vector3.Lerp(transform.position, tileSpawner.tileList[currentPos].transform.position, Time.deltaTime * 50f);       
                    yield return null;
                }
                yield return null;
            }
            targetPos -= temp;
            while (transform.position != tileSpawner.tileList[targetPos].transform.position)
            {
                currentPos--;
                while(transform.position != tileSpawner.tileList[currentPos].transform.position)
                {
                    transform.position = Vector3.Lerp(transform.position, tileSpawner.tileList[currentPos].transform.position, Time.deltaTime * 50f);       
                    yield return null;
                }
                yield return null;
            }
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class TileSpawner : MonoBehaviour
{
    private enum TileType {normal, snake, ladder}
    [SerializeField] private GameObject tile;
    private const int size = 10;
    [SerializeField] private GameObject tileParent;
    [SerializeField] private float distance;
    [SerializeField] private Vector3 startPos;
    public GameObject[,] tiles;
    public List<GameObject> tileList;
    private int index = 0;
    Dictionary<int,TileType> list = new Dictionary<int, TileType>();

    void Start()
    {
        tiles = new GameObject[size,size];
        for (int z = 0; z < size; z++)
        {
                for (int x = 0; x < size; x++, index++)
                {
                    StartCoroutine(TileSpawn(x,z,index));
                }
                z++;
                for (int x = 9; x > -1; x--, index++)
                {
                    StartCoroutine(TileSpawn(x,z,index));
                }
        }
    }
    IEnumerator TileSpawn(int x, int z, int index)
    {
        yield return new WaitForSeconds(z*0.1f);
        tiles[x,z] = Instantiate(tile, new Vector3(startPos.x + (x*distance), 0, startPos.z + (z*distance)), Quaternion.identity);
        tiles[x,z].transform.SetParent(tileParent.transform);
        tiles[x,z].name = index.ToString();
        tileList.Add(tiles[x,z]);
    }
}

